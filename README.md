# R scripts for the analysis of PTMomics data #

These files were created for the book chapter _Computational and Statistical Methods for High-Throughput Mass Spectrometry based PTMs Analysis_ in the book _Protein Bioinformatics: from Protein Modifications and Networks to Proteomics_ of the Springer Series _Methods in Molecular Biology_.